const platformClasses = {
    "itch": "itch-btn",
    "steam": "steam-btn",
    "xbox": "xbox-btn",
    "ps4": "ps4-btn",
    "epic": "epic-games-btn",
    "pc": "pc-btn",
    "oculus": "oculus-btn",
    "sidequest": "sidequest-btn"
};

const getPlatformClass = (keyName) => {
  try {
    return platformClasses[keyName];
  } catch {
    return '';
  }
};

export default getPlatformClass;
