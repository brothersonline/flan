import React, {useState} from 'react';
import getPlatformClass from "../Functions/classes";
import gamesData from "../gamesData";
import GamesList from "./GamesList";

function chunkArray(myArray, chunk_size){
  var results = [];

  while (myArray.length) {
    results.push(myArray.splice(0, chunk_size));
  }

  return results;
}

const activeFilterIsFoundInObject = (ItemProperty, activeFilters) => {
  for (let i = 0; i < activeFilters.length; i++) {
    if(ItemProperty.hasOwnProperty(activeFilters[i])) {
      return true;
    }
  }
  return false;
};

const activeFilterIsFoundInArray = (ItemProperty, activeFilters) => {
  for (let i = 0; i < activeFilters.length; i++) {
    if(ItemProperty.includes(activeFilters[i])) {
      return true;
    }
  }
  return false;
};

const filterResultsOnInput = (gamesList, inputFilterString) => {
  return gamesList.filter(function (item) {
    return item.name.toLowerCase().includes(inputFilterString.toLowerCase());
  });
};

const filterResultsOn = (playMethods, gamesList, activePlayMethodFilters) => {
  return gamesList.filter(function(item) {
    return activeFilterIsFoundInArray(item[playMethods], activePlayMethodFilters);
  });
};

const filterResultsOnStores = (gamesList, activeStoresFilters) => {
  return gamesList.filter(function(item) {
    return activeFilterIsFoundInObject(item.platforms, activeStoresFilters);
  });
};

function sortListByName(filteredGameRows) {
  return filteredGameRows.sort((a, b) => (a.name > b.name) ? 1 : -1)
}

function DefaultView({setActiveView}) {
  const [inputFilter, setInputFilter] = useState("");
  const [activePlatformFilter, setActivePlatformFilter] = useState("pc");
  const [activeStoresFilters, setActiveStoresFilters] = useState(gamesData.allPlatforms.pc.stores);
  const [activePlayMethodFilters, setActivePlayMethodFilters] = useState(gamesData.allPlayMethods);
  const [activeInstallSizesFilters, setActiveInstallSizesFilters] = useState(gamesData.allInstallSizes);

  function updateInputFilter(e){
    setInputFilter(e.target.value);
  }

  function togglePlatformFilter(filterString){
    setActivePlatformFilter(filterString);
    setActiveStoresFilters(gamesData.allPlatforms[filterString].stores);
  }

  function toggleFilter(filterString, filterStateVar, setFilterStateFunc, ){
    const newActiveStateFilters = filterStateVar.slice(0);
    if(filterStateVar.includes(filterString)) {
      newActiveStateFilters.splice(newActiveStateFilters.indexOf(filterString), 1);
    } else {
      newActiveStateFilters.push(filterString);
    }

    setFilterStateFunc(newActiveStateFilters);
  }

  let filteredGameRows = gamesData.games.slice(0);
  filteredGameRows = filterResultsOnInput(filteredGameRows, inputFilter);
  filteredGameRows = filterResultsOnStores(filteredGameRows, activeStoresFilters);
  filteredGameRows = filterResultsOn('playMethods', filteredGameRows, activePlayMethodFilters);
  filteredGameRows = filterResultsOn('installSize', filteredGameRows, activeInstallSizesFilters);
  filteredGameRows = sortListByName(filteredGameRows);
  filteredGameRows = chunkArray(filteredGameRows,3);

  return (
    <>
    <header id="home">
      <section className="hero">
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-md-offset-2 text-center">
              <img className="animated fadeInDown hero-heading-image" src={`${process.env.PUBLIC_URL}/img/flan-logo.png`} alt="logo"/>
            </div>
          </div>
          <div className="row text-center animated fadeInDown">
            <div className="col-md-12 wp4">
              <input className={"hero-filter-input"} placeholder={"Search for a game"} onChange={updateInputFilter}/>
            </div>
          </div>
          <div className="row text-center animated fadeInUp hero-button-row">
            <div className="col-md-12 wp4">
              {Object.keys(gamesData.allPlatforms).map((platform) => (
                <button
                  key={platform}
                  className={`btn ${getPlatformClass(platform)} ${activePlatformFilter === platform ? "" : "inactive-filter-btn"}`}
                  onClick={() => togglePlatformFilter(platform)}>{platform}</button>
              ))}
            </div>
          </div>
          <div className="row text-center animated fadeInUp">
            <div className="col-md-12 wp4">
              {gamesData.allPlayMethods.map((playMethod) => (
                <button
                  key={playMethod}
                  className={`btn ${playMethod} ${activePlayMethodFilters.includes(playMethod) ? "" : "inactive-filter-btn"} small-btn`}
                  onClick={() => toggleFilter(playMethod, activePlayMethodFilters, setActivePlayMethodFilters)}>{playMethod}</button>
              ))}
            </div>
          </div>
          <div className="row text-center animated fadeInUp">
            <div className="col-md-12 wp4">
              {gamesData.allInstallSizes.map((installSize) => (
                <button
                  key={installSize}
                  className={`btn ${activeInstallSizesFilters.includes(installSize) ? "" : "inactive-filter-btn"} small-btn install-size-btn ${installSize}`}
                  onClick={() => toggleFilter(installSize, activeInstallSizesFilters, setActiveInstallSizesFilters)}>{installSize}</button>
              ))}
            </div>
          </div>
          {gamesData.allPlatforms[activePlatformFilter].stores.length > 1 &&  (
            <div className="row text-center animated fadeInUp">
              <div className="col-md-12 wp4 stores-button-row">
                {gamesData.allPlatforms[activePlatformFilter].stores.map((store) => (
                  <button
                    key={store}
                    className={`btn ${getPlatformClass(store)} ${activeStoresFilters.includes(store) ? "" : "inactive-filter-btn"} small-btn`}
                    onClick={() => toggleFilter(store, activeStoresFilters, setActiveStoresFilters)}>{store}</button>
                ))}
              </div>
            </div>
          )}
        </div>
      </section>
      <button className="btn view-change-btn" onClick={() => setActiveView(1)}><i className="fa fa-trophy"></i></button>
    </header>
    <section className="games text-center section-padding">
      <GamesList filteredGameRows={filteredGameRows} />
    </section>
  </>
  );
}

export default DefaultView;
