import React from 'react';
import getPlatformClass from "../Functions/classes";
import LazyImage from "./LazyImage";

const snakeCase = string => {
  return string.toLowerCase().replace(/\W+/g, " ")
    .split(/ |\B(?=[A-Z])/)
    .map(word => word.toLowerCase())
    .join('_');
};

const getImageSrc = (imageName) => {
    return `${process.env.PUBLIC_URL}/gameImages/${imageName}.jpg`;
};

function GamesList({filteredGameRows}) {
  return (
    <div className="container">
      {filteredGameRows.length === 0 &&
        <div>No games where found with the current filters</div>
      }
      {filteredGameRows.map((filteredGames, i) => {
        return (
          <div className="row" key={`row-${i}`}>
            {filteredGames.map((filteredGame) => {
              const easyName = snakeCase(filteredGame.name);
              return (
                <div className="col-md-4 wp4 delay-05s" key={easyName}>
                  <LazyImage className="game-image" unloadedSrc={`${process.env.PUBLIC_URL}/img/placeholder-image.png`} src={getImageSrc(easyName)} alt="Game banner" />
                  <h2>{filteredGame.name}</h2>
                  <p>Install size: <span className={`btn small-btn unclickable install-size-btn ${filteredGame.installSize}`}>{filteredGame.installSize}</span></p>
                  <div className={"buttons-container"}>
                    {Object.keys(filteredGame.platforms).map((keyName) => {
                      return(
                        <a key={keyName} href={filteredGame.platforms[keyName]} className={`btn ${getPlatformClass(keyName)}`} target="_blank" rel='noreferrer noopener'>{keyName}</a>
                      )})}
                  </div>
                </div>
              )
            })}
          </div>
        )
      })}
    </div>
  );
}

export default GamesList;
