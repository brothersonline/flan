import React, {useState} from 'react';
import tournamentsData from "../tournamentData";

function TournamentView({setActiveView}) {
  const [activeTournamentIndex, setActiveTournamentIndex] = useState(0);

  const activeTournament = tournamentsData.tournaments[activeTournamentIndex];

  return (
    <>
      <header id="tournament">
        <section className="hero">
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-md-offset-2 text-center">
                <h1 className="animated fadeInDown delay-05s hero-subheading">Tournaments</h1>
              </div>
            </div>
            <div className="row text-center animated fadeInUp hero-button-row">
              <div className="col-md-12 wp4">
                {tournamentsData.tournaments.map((tournament, i) => (
                  <button
                    key={tournament.name}
                    className={`btn ${activeTournamentIndex === i ? "" : "inactive-filter-btn"}`}
                    onClick={() => setActiveTournamentIndex(i)}>{tournament.name}</button>
                ))}
              </div>
            </div>
            <div className="row text-center animated fadeInUp hero-button-row">
              <div className="col-md-12 wp4">
                {activeTournament.games.map((game, i) => (
                  <a href={`#${game.name}`} className={"btn small-btn"}>{game.name}</a>
                ))}
              </div>
              <div className="col-md-12 wp4">
                <a className={"btn challonge-btn"} href={activeTournament.url}>Event Page</a>
              </div>
            </div>
          </div>
        </section>
        <button className="btn view-change-btn" onClick={() => setActiveView(0)}><i className="fa fa-gamepad"></i></button>
      </header>
      <section className="tournaments text-center section-padding">
        {activeTournament.games.map((game, i) => (
          <div id={game.name}>
            <h1>{game.name}</h1>
            <iframe title="iframe" src={game.challongeBracketUrl} width="100%" height="500px" id="iFrameResizer0" scrolling="no" style={{overflow: "hidden"}}></iframe>
          </div>
        ))}
      </section>
    </>
  );
}

export default TournamentView;
