import React, {useState} from 'react';
import TournamentView from "./Components/TournamentView";
import DefaultView from "./Components/DefaultView";

function App() {
  const [activeView, setActiveView] = useState(0);

  return (
    <div className="App">
      {activeView === 1 && (<TournamentView setActiveView={setActiveView} />)}
      {activeView === 0 && (<DefaultView setActiveView={setActiveView} />)}
    </div>
  );
}

export default App;
